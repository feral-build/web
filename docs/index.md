---
title: ''
---

- [Flashing Firmware for the Buchla LEM218v3 on Linux, 2023-12-25](posts/2023-12-25-flashing-firmware-lem218-linux.md)
- [Building the Drone of Doom, 2023-12-07](posts/2023-12-07-drone-synth.md)
- [Render partial templates with Ansible, 2023-08-29](posts/2023-08-29-ansible-partial-templates.md)
