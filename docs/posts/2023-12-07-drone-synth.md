---
title: "Building the Drone of Doom"
date: 2023-12-07
draft: false
---

2023-12-07

We recently built Look Mum No Computer's [2002 Oscillator Drone of Doom](https://www.lookmumnocomputer.com/projects#/circledroneofdoom)
from a PCB/panel kit. Released in mid 2022, the synth incorporates a few fun design features.

 ![Drone PCB, setting up for assembly](img/2023-12-07-drone-synth/wip-start.jpg){: class="centered-image" style="max-height: 300px"}

# overview

The circular format of this instrument is a welcome break from the usual "it's a rectangle" synths you usually see.
With three oscillator tune knobs and switches on each side, this is a fun platform for one or two people to
explore drones and textures. There are no CV inputs, so this is very much a hands-on instrument.

# sound

<iframe width="100%" height="150" scrolling="no" frameborder="no" src="https://open.audio/front/embed.html?&amp;type=album&amp;id=20614"></iframe>

The default tones of the drone can be quite harsh. A low pass filter will take care of this harshness.
Modulating the cutoff or resonance of the filter creates an interesting effect where harmonics swell and
subside. You can hear this happening in the tune above.

# build

With a relatively limited number of through-hole components, this instrument took about four hours to plan and build.
In prep for the build, we added the bill of materials to https://partsbox.com and read through
[the discourse thread](https://lookmumnocomputer.discourse.group/t/2002-oscillator-drone-of-doom/4854/14)
to learn about any gotchas in the build process.

From that thread, we decided to use different timing capacitors to alter the pitch range,
splitting the six oscillators into three pairs - high, medium and low. We used knobs of different colors
to signal the pitch of each oscillator. Seeing pitch range at a glance is helpful for performance.

|capacitor|pitch|knob color|
|-|-|-|
|2.2uF|high|green|
|4.7uF|medium|yellow|
|10uF|low|red|

## reverse avalanche breakdown

The oscillators for this instrument rely on a fun effect of bipolar junction transistors (BJTs)
called reverse avalanche breakdown, where connecting the transistor backwards turns it into a pulse generator.
Apply more voltage to push pulse generation into audio rates and you have yourself an analog oscillator.
Much more on this topic, including example circuits, in Kerry D. Wong's [BJT In Reverse Avalanche Mode blog post](http://www.kerrywong.com/2014/03/19/bjt-in-reverse-avalanche-mode/).
Also see LMNC's [post on breadboarding a reverse avalance oscillator](https://www.lookmumnocomputer.com/simplest-oscillator).

 ![waveform from drone on oscilloscope](img/2023-12-07-drone-synth/oscwave.jpg){: class="centered-image" style="max-height: 300px"}

Alas not all transistors are created equal.
To run this instrument from a standard 12 volt DC wall wart, the transistors must have a lower `Vbe`, or reverse breakdown voltage.
[SS9018s](https://www.componentsinfo.com/ss9018-transistor-pinout-equivalent/) are chosen for this build because they start to oscillate at 8 volts. We breadboarded the circuit to test [some 2N3904 transistors]() we had in stock, and they did start to oscillate, but only above 14 volts and while using a 1uF timing capacitor. SS9018 it is, bought 100 pack of `SS9018HBU` on ebay for $14.

The first volt or so above 9v of the supply voltage will result in a low frequency oscillator - below our hearing range but creatively useful.
The 10uF low oscillators in this build only become audible after about 1/3 turn of the pot. Below that you can hear clicks
and pops as pulses are generated. This can result in interesting rhythmic patterns, as heard in the end of the tune embedded above.

# end

 ![waveform from drone on oscilloscope](img/2023-12-07-drone-synth/final.jpg){: class="centered-image" style="max-height: 300px"}

The Drone of Doom is a fun project with low part count, good for beginner or intermediate builders.
With 94 SS9018s left over, we'll be building more transistor oscillators in the future. Thanks for reading.
