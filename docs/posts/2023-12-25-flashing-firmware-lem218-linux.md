---
title: "Flashing Firmware for the Buchla LEM218v3 on Linux"
date: 2023-12-25
draft: false
---

2023-12-25

The firmware download provided on the [Buchla LEM218v3 product page](https://buchla.com/lem218/)
instructs how to update the firmware on Windows or MacOS, but since the process uses standard
open source tools the firmware can be updated from a Linux machine as well.

 ![LEM218v3, top view](img/LEM218v3-top.jpg){: class="centered-image size80pct"}

# steps

## 1. Download and install tools

Download the latest Linux release of SendMIDI from https://github.com/gbevin/SendMIDI/releases.
Extract the binary from the archive and place on your `PATH`.
At this time, the latest release with pre-built Linux binaries is `1.1.0`, which works fine for this task.

Download the latest Linux release of dfu-programmer from https://github.com/dfu-programmer/dfu-programmer/releases.
Extract the binary from the archive and place on your `PATH`.


## 2. Place LEM218 in DFU mode

Run the following commands to place the LEM in DFU/Device Firmware Upgrade mode:

```sh
# put LEM in dfu mode
sendmidi dec
sendmidi dev 218e syx 0 2 55 2 1 1
```

You can ensure the LEM is in firmware mode by running `dfu-util -l` to list devices.
The LEM should be in this list before moving onto next step.

## 3. Flash new firmware

Download and extract the `218ev3-Firmware-Flashing.zip` file from https://buchla.com/lem218/.
The firmware file to flash is `218eV3_v362_DFU.hex`, found in either the `mac/` or `windows/`
folders of this extracted file. Both firmware hex files are the same.

Run the following commands to flash the new firmware.
Update `FIRMWARE_FILE` to point to where the hex file is downloaded on your system.

```sh
FIRMWARE_FILE="$HOME/Downloads/218ev3-Firmware-Flashing/mac/218eV3_v362_DFU.hex"

# erase current firmware
dfu-programmer at32uc3b1256 erase

# program new firmware
dfu-programmer at32uc3b1256 flash --suppress-bootloader-mem "$FIRMWARE_FILE"

# restart LEM
dfu-programmer at32uc3b1256 start
```

---

Your LEM218 is now running the latest firmware. Enjoy!
