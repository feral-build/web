FROM python:3.10-slim

RUN adduser --system --home /usr/src/app appuser
USER appuser

WORKDIR /usr/src/app

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY mkdocs.yml .
COPY docs ./docs

RUN /usr/src/app/.local/bin/mkdocs build

EXPOSE 8000
CMD ["python", "-m", "http.server", "8000", "-d", "./site"]
