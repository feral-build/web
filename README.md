# pages

web site

## about

uses https://www.mkdocs.org, theme https://github.com/FernandoCelmer/mkdocs-simple-blog

## usage

- `./scripts/run`: run site on http://localhost:8000
- `./scripts/build-site`: build site to `site/` dir
    - Set `PUBLISH=1` to deploy to public pages site
